package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.util.TerminalUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-create";
    }

    @Override
    public @NotNull String description() {
        return "Create project";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (isEmpty(name)) throw new EmptyNameException();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().add(userId, name, description);
    }

}
