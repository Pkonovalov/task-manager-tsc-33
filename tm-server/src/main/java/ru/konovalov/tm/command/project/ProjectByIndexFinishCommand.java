package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIndexFinishCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-finish-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Finish project by index";
    }

    @Override
    public void execute() {
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().finishProjectByIndex(userId, TerminalUtil.nextNumber());
    }

}
