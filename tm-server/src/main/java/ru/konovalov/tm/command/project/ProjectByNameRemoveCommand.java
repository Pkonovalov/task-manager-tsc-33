package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByNameRemoveCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        serviceLocator.getProjectTaskService().removeProjectByName(userId, TerminalUtil.nextLine());
    }

}
