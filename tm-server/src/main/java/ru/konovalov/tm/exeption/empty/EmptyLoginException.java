package ru.konovalov.tm.exeption.empty;

import ru.konovalov.tm.exeption.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error. Login is empty...");
    }

}
