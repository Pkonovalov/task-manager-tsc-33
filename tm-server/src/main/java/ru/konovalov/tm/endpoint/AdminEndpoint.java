package ru.konovalov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.endpoint.IAdminEndpoint;
import ru.konovalov.tm.api.service.ServiceLocator;
import ru.konovalov.tm.component.Backup;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    private Backup backup;

    public AdminEndpoint() {
        super(null);
    }

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final Backup backup
    ) {
        super(serviceLocator);
        this.backup = backup;
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.load();
    }

    @Override
    @WebMethod
    public void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.loadJson();
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.run();
    }

    @Override
    @WebMethod
    public void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        backup.saveJson();
    }

}
