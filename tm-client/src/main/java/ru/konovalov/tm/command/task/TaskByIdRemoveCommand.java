package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.TerminalUtil;

public final class TaskByIdRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by id";
    }

    @Override
    public void execute() {
        User user = serviceLocator.getAuthService().getUser();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        serviceLocator.getTaskService().removeOneById(user.getId(), TerminalUtil.nextLine());
    }

}


