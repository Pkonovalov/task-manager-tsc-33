package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.User;

public interface IAuthService {

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role... roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);
}
