package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IOwnerService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;

public interface IProjectService extends IOwnerService<Project> {
    @NotNull
    Project add(@NotNull String userId, @NotNull String name, @NotNull String description);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Project removeProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByIndex(String userId, Integer index);

    @NotNull
    Project updateProjectByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @Nullable String description);

    @NotNull
    Project updateProjectById(String userId, String id, String name, String description);

    @NotNull
    Project updateProjectByName(@NotNull String userId, @NotNull String name, @NotNull String nameNew, @Nullable String description);

    @NotNull
    Project startProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project startProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project startProjectByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project finishProjectById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project finishProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    Project finishProjectByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Project changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    Project changeProjectStatusByName(@NotNull String userId, @Nullable String name, @Nullable Status status);

    @Nullable
    Project changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    boolean existsByName(String userId, String name);

    void setProjectStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    void setProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    void setProjectStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

}
